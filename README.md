# KBD
## Project Overview
This project is designed to enable a second keyboard to function as a fully programmable macro-board as a self-contained application.
## Platform
This repository is being developed and maintained using the **Linux kernel**.<br />
It's ability to function multi-platform has **_not_** been tested and thus should be assumed non-functional at this time
## Languages
This project is written in C++ and uses Qt for the GUI. <br />
All code in this repository is under GNU GPLv3.0 licencing
